(function () {
    'use strict';

    angular.module('letsAngular')
        .directive('fwDynamic', fwDynamic);

    fwDynamic.$inject = ['viaCEP', '$timeout', '$compile', 'jQuery', '$filter'];

    function fwDynamic(viaCEP, $timeout, $compile, jQuery, $filter) {
        var FLOAT_REGEXP_1 = /^\$?\d+.(\d{3})*(\,\d*)$/; //Numbers like: 1.123,56
        var FLOAT_REGEXP_2 = /^\$?\d+,(\d{3})*(\.\d*)$/; //Numbers like: 1,123.56
        var FLOAT_REGEXP_3 = /^\$?\d+(\.\d*)?$/; //Numbers like: 1123.56
        var FLOAT_REGEXP_4 = /^\$?\d+(\,\d*)?$/; //Numbers like: 1123,56

        return {
            restrict: 'A',
            link: {
                post: function postLink(scope, $el, attrs, controller) {
                    if (!controller) {
                        controller = $el.controller('ngModel');
                    }

                    if (scope.field.type == 'date') {
                        $el.mask('99/99/9999');

                    } else if (scope.field.customOptions.cpf != undefined) {
                        $el.mask('999.999.999-99');

                    } else if (scope.field.customOptions.cnpj != undefined) {
                        $el.mask('99.999.999/9999-99');

                    } else if (scope.field.type == 'float') {
                        if (scope.field.customOptions.currency != undefined) {
                            $el.mask("#.##0,00", { reverse: true });
                            controller.$parsers.unshift(function (value) {
                                return parseFloat($el.cleanVal(), 10) / 100;
                            });
                            controller.$formatters.unshift(function (value) {
                                return $el.masked(value ? parseFloat(value).toFixed(2) : null);
                            });
                        } else {

                        }
                    } else if (scope.field.customOptions.documento !== undefined) {

                        var cpfOrCnpj = function (val) {
                            return val.replace(/\D/g, '').length >= 12 ? '00.000.000/0000-00' : '000.000.000-009';
                        },
                            docOptions = {
                                onKeyPress: function (val, e, field, options) {
                                    field.mask(cpfOrCnpj.apply({}, arguments), options);
                                    if (val.replace(/\D/g, '').length >= 13) {
                                        var cnpj = val;
                                        var controller = false;

                                        cnpj = cnpj.replace(/[^\d]+/g, '');
                                        if (cnpj == '') {
                                            scope.validator = false;
                                            controller = true;
                                        }
                                        if (cnpj.length != 14) {
                                            scope.validator = false;                                            
                                            controller = true;
                                        }
                                        // Elimina CNPJs invalidos conhecidos
                                        if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" ||
                                            cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" ||
                                            cnpj == "88888888888888" || cnpj == "99999999999999") {
                                            scope.validator = false;                                            
                                            controller = true;
                                        }

                                        // Valida DVs
                                        var tamanho = cnpj.length - 2
                                        var numeros = cnpj.substring(0, tamanho);
                                        var digitos = cnpj.substring(tamanho);
                                        var soma = 0;
                                        var pos = tamanho - 7;
                                        for (var i = tamanho; i >= 1; i--) {
                                            soma += numeros.charAt(tamanho - i) * pos--;
                                            if (pos < 2)
                                                pos = 9;
                                        }
                                        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                                        if (resultado != digitos.charAt(0)) {
                                            scope.validator = false;                                            
                                            controller = true;
                                        }

                                        tamanho = tamanho + 1;
                                        numeros = cnpj.substring(0, tamanho);
                                        soma = 0;
                                        pos = tamanho - 7;
                                        for (var i = tamanho; i >= 1; i--) {
                                            soma += numeros.charAt(tamanho - i) * pos--;
                                            if (pos < 2)
                                                pos = 9;
                                                controller = true;
                                        }
                                        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                                        if (resultado != digitos.charAt(1)){
                                            scope.validator = false;                                            
                                            controller = true;
                                        }
                                        if (!controller) {
                                            scope.validator = true;                                            
                                        }
                                    }
                                }
                            };

                        $timeout(function () {
                            $el.mask(cpfOrCnpj, docOptions);
                        }, 10);


                    } else if (scope.field.customOptions.telefone != undefined) {
                        var SPMaskBehavior = function (val) {
                            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                        },
                            spOptions = {
                                onKeyPress: function (val, e, field, options) {
                                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                                }
                            };

                        $timeout(function () {
                            $el.mask(SPMaskBehavior, spOptions);
                        }, 100);

                    } else if (scope.field.customOptions.cep != undefined) {
                        $el.mask('99999-999');
                        $el.blur(function () {

                            if (!this.value) {
                                return false;
                            }

                            var $scope = angular.element(this).scope();
                            var dataVar = jQuery(this).parent().attr('fw-data');
                            viaCEP.get(this.value).then(function (response) {
                                var map = $scope.field.customOptions.cep;

                                $scope.data[map.address] = response.logradouro;
                                $scope.data[map.district] = response.bairro;
                                $scope.data[map.city] = response.localidade;
                                $scope.data[map.state] = response.uf;
                                $scope.data[map.ibge] = response.ibge;
                                $scope.data[map.gia] = response.gia;
                            });
                        });
                    }
                }
            }
        }
    }
})();
