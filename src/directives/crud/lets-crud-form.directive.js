(function () {
  'use strict';

  angular.module('letsAngular')
    .directive('crudForm', crudForm);

  crudForm.$inject = ['jQuery', '$timeout', 'appSettings', 'ngToast', 'SweetAlert'];

  function crudForm(jQuery, $timeout, appSettings, ngToast, SweetAlert) {
    return {
      replace: false,
      link: function (scope, $el, element) {
        var sending = false;
        var backupName;
        // /* Dropzone */
        // //Set options for dropzone
        var status_sending = false;
        for (var y in scope.headers.fields) {
          var field = scope.headers.fields[y];
          if (field.customOptions.file) {
            scope.dzOptions = {
              url: appSettings.API_URL + 'upload/' + field.customOptions.file.container + '/upload',
              maxFilesize: '60',
              maxFiles: '1',
              uploadMultiple: false,
              addRemoveLinks: true,
              acceptedFiles: field.customOptions.acceptedFiles || null,
              renameFilename: function (filename) {
                if (!sending) {
                  sending = true;
                  backupName = ( field.customOptions.categoria ? (field.customOptions.categoria + '_') : '' ) +  new Date().getTime() + '_'+ filename;
                } else {
                  sending = false;
                }
                return backupName;
              },
              complete: function (file) {
                sending = false;            
                // SweetAlert.success("Upload realizado com sucesso!")
                // ngToast.success('Upload realizado com sucesso!')
                $(".dz-progress").css("display", "none");
                $(".dz-success-mark svg").css("background", "green");
                $(".dz-error-mark").css("display", "none");
                
              },
              error: function (file, response) {
                // console.log(response);
                
                // ngToast.warning(response);
                SweetAlert.error("Aconteceu um erro!")
                // console.log(SweetAlert.error('Crasho tudo meu amigão','Crasho tudo meu amigão'));
                
                angular.element('.dz-remove')[angular.element('.dz-remove').length - 1].click(function () {});
                sending = false;
              },

            };
          }
        }
        //Apply methods for dropzone
        // scope.dzMethods = {};
        // scope.removeNewFile = function () {
        //   console.log('AQUI');

        //   scope.dzMethods.removeFile(scope.newFile); //We got $scope.newFile from 'addedfile' event callback
        // }
        /* Dropzone */

        jQuery($el).on('click', '.button-new', function () {
          var detail = jQuery(this).attr('detail');
          var origin = jQuery(this).attr('origin');
          var modal = jQuery(this).attr('form-modal') == "true";

          scope.newDetailData(origin, detail, modal);
        });

        $timeout(function () {
          $el.find('.tab-group .nav-tabs li:first').find('a').click();
          $el.find(':input[type!="hidden"]:first').focus();
        }, 500);

        $($el).parsley({
          priorityEnabled: true,
          errorsContainer: function (el) {
            return el.$element.closest(".input-container");
          }
        });

      }
    }
  }
})();
